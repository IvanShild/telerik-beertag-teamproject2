package com.beertag.project.controllers;

import com.beertag.project.models.User;
import com.beertag.project.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;

@Controller
public class AdminController {
    private UserService userService;

    @Autowired
    public AdminController(UserService userService) {
        this.userService = userService;
    }
    @GetMapping("/admin")
    public String showAdminPage() {
        return "admin";
    }

    @PutMapping("/admin/user/{name}")
    public String showEditUser(Model model,@RequestParam String name, @RequestBody @Valid User user) {
        try {
            userService.update(name, user);
            model.addAttribute("userProfile", user);
            return "userProfile";
        } catch (RuntimeException e) {
        throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }


    //TODO HOW DID YOU MAKE IT?
    @DeleteMapping("/admin/user/{name}")
    public String showDeleteUser(@RequestParam String name) {
        try {
        userService.delete(name);
        return "admin";
        } catch (RuntimeException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }
}
