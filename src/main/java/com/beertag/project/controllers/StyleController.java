package com.beertag.project.controllers;

import com.beertag.project.models.Beer;
import com.beertag.project.models.Style;
import com.beertag.project.services.StyleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;

@Controller
public class StyleController {
    private StyleService styleService;

    @Autowired
    public StyleController(StyleService styleService) {
        this.styleService = styleService;
    }

    @GetMapping("/styles")
    public List<Style> getAll(){
        return styleService.getAll();
    }

    @GetMapping("/{id}/beers")
    public List<Beer> getBeers(@PathVariable int id) {
        return styleService.getById(id).getBeers();
    }
}

