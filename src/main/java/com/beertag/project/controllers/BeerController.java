package com.beertag.project.controllers;

import com.beertag.project.models.Beer;
import com.beertag.project.models.CountryOfOrigin;
import com.beertag.project.models.Style;
import com.beertag.project.models.Tag;
import com.beertag.project.services.BeerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@Controller
public class BeerController {

    private BeerService beerService;

    @Autowired
    public BeerController(BeerService beerService) {
        this.beerService = beerService;
    }

//    @GetMapping("/beers")
//    public String getAll(Model model) {
//        List<Beer> beers = beerService.getAll();
//        model.addAttribute("beers", beers);
//        return "beers";
//    }

    @GetMapping("/beers/name")
    public String getByName(Model model) {
        List<Beer> beers = beerService.sortByName();
        model.addAttribute("beers", beers);
        return "beers";
    }

    @GetMapping("/beers/rating")
    public String getByRating(Model model) {
        List<Beer> beers = beerService.sortByAverageRating();
        model.addAttribute("beers", beers);
        return "beers";
    }

    @GetMapping("/beers/abv")
    public String getByAbv(Model model) {
        List<Beer> beers = beerService.sortByABV();
        model.addAttribute("beers", beers);
        return "beers";
    }

    @GetMapping("/beers/country")
    public String getbyCountry(Model model, @RequestBody String name) {
        List<Beer> beers = beerService.getByCountry(name);
        model.addAttribute("beers", beers);
        return "beers";
    }

    @GetMapping("/beers/create")
    public String getCreateBeer(Model model) {
        List<CountryOfOrigin> countries = beerService.getAllCountries();
        List<Style> styles = beerService.getAllStyles();
        model.addAttribute("beer", new Beer());
        model.addAttribute("countries", countries);
        model.addAttribute("styles", styles);

        return "beerCreateV2";
    }

    @GetMapping("/beers/style")
    public String getbyStyle(Model model, @RequestBody String name) {
        List<Beer> beers = beerService.getByStyle(name);
        model.addAttribute("beers", beers);
        return "beers";
    }

    @GetMapping("/beers/tags")
    public String getbyTag(Model model, @RequestBody List<String> tags) {
        Set<Beer> beers = beerService.getByTag(tags);
        model.addAttribute("beers", beers);
        return "beers";
    }

    @PostMapping("/beers/create")
    public String create(@Valid @ModelAttribute Beer beer, BindingResult bindingResult, Model model) {

        System.out.println(beer.getBeerName());

        try {
//            if (bindingResult.hasErrors()) {
//                model.addAttribute("error", "Try again!");
//                return "beerCreateV2";
//            }
            beerService.create(beer);
            model.addAttribute("beer", beer);
            return "beerProfile";
        } catch (RuntimeException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @GetMapping("/beers/{id}")
    public String getById(Model model, @PathVariable int id) {
        try {
            Beer beer = beerService.getById(id);
            model.addAttribute("beer", beer);
            return "beerProfile";
        } catch (RuntimeException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PutMapping("/beers/{id}")
    public String update(Model model, @PathVariable int id, @RequestBody @Valid Beer beer) {
        try {
            beerService.update(id, beer);
            model.addAttribute("beerProfile", beer);
            return "beerProfile";
        } catch (RuntimeException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

    @DeleteMapping("/beers/{id}")
    public String delete(@PathVariable int id) {
        try {
            beerService.delete(id);
            return "beers";
        } catch (RuntimeException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping("/beers")
    public String showBeersPage(Model model,
                               @RequestParam(value = "style", required = false) String beerStyle,
                               @RequestParam(value = "country", required = false) String countryOfOrigin,
                               @RequestParam(value = "tag", required = false) String tag) {
        try {

        List<Beer> beers;

        if (beerStyle != null){
            beers = beerService.getBeersByStyle(beerStyle);
        }
        else if(countryOfOrigin != null){
            beers = beerService.getBeersByCountry(countryOfOrigin);
        }
        else if (tag  != null){
             beers = beerService.getBeersByTags(tag);
        }
        else{
          beers = beerService.getAll();
        }

        model.addAttribute("beers", beers);
        return "beers";

        } catch (RuntimeException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }
}

//        List<Beer> sortedBeers;
//List<BeerDTO> sortedDTOBeers;

//        if (sortBy != null) {

//            switch (sortBy) {
//                case "ABV":
//                    sortedBeers = beerService.sortByABV(beers);
//                    model.addAttribute("beers", sortedBeers);
//                    break;
//                case "name":
//                    sortedBeers = beerService.sortByName(beers);
//                    model.addAttribute("beers", sortedBeers);
//                    break;
//                case "rating":
//                    sortedBeers = beerService.sortByAverageRating(beers);
//                    model.addAttribute("beers", sortedBeers);
//                    break;
//            }
//        }