package com.beertag.project.controllers;

import com.beertag.project.models.Tag;
import com.beertag.project.services.TagService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;

@Controller
public class TagController {
    private TagService tagService;

    @Autowired
    public TagController(TagService tagService) {
        this.tagService = tagService;
    }

    @GetMapping("/tags")
    public String getAll(Model model) {
        List<Tag> tags = tagService.getAll();
        model.addAttribute("tags", tags);
        return "tags";
    }

    @PostMapping("/tags/create")
    public Tag create(Model model, @RequestBody Tag tag) {
        try {
            tagService.create(tag);
            model.addAttribute("tag", tag);
            return tag;
        } catch (RuntimeException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @GetMapping("/tags/{id}")
    public String getById(Model model, @PathVariable int id) {
        try {
            Tag tag = tagService.getById((id));
            model.addAttribute("tag", tag);
            return "tag";
        } catch (RuntimeException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }


}
