package com.beertag.project.controllers.RESTControllers;

import com.beertag.project.models.Beer;
import com.beertag.project.services.BeerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/beers")
public class RESTBeerController {

    private BeerService beerService;

    @Autowired
    public RESTBeerController(BeerService beerService) {
        this.beerService = beerService;
    }

    @PostMapping
    public Beer create(@RequestBody @Valid Beer beer) {
        try {
            beerService.create(beer);
            return beer;
        } catch (RuntimeException e) {
            throw new ResponseStatusException(
                    HttpStatus.CONFLICT,
                    e.getMessage()
            );
        }
    }

    @GetMapping
    public List<Beer> getAll() {
        return beerService.getAll();
    }

    @GetMapping("/{id}")
    public Beer getById(@PathVariable int id) {
        try {
            return beerService.getById(id);
        } catch (RuntimeException e) {
            throw new ResponseStatusException(
                    HttpStatus.NOT_FOUND,
                    e.getMessage()
            );
        }
    }

    @PutMapping("/{id}")
    public Beer update(@PathVariable int id, @RequestBody @Valid Beer beer) {
        try {
            beerService.update(id, beer);
            return beer;
        } catch (RuntimeException e) {
            throw new ResponseStatusException(
                    HttpStatus.BAD_REQUEST,
                    e.getMessage()
            );
        }
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable int id) {
        try {
            beerService.delete(id);
        } catch (RuntimeException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }


}
