package com.beertag.project.controllers.RESTControllers;

import com.beertag.project.models.Beer;
import com.beertag.project.models.Style;
import com.beertag.project.services.StyleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/styles")
public class RESTStyleController {
    private StyleService styleService;

    @Autowired
    public RESTStyleController(StyleService styleService) {
        this.styleService = styleService;
    }

    @GetMapping
    public List<Style> getAll() {
        return styleService.getAll();
    }

    @GetMapping("/{id}/beers")
    public List<Beer> getBeers(@PathVariable int id) {
        return styleService.getById(id).getBeers();
    }
}