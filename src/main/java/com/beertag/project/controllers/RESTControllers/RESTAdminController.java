package com.beertag.project.controllers.RESTControllers;

import com.beertag.project.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/admin")
public class RESTAdminController {

    private UserService userService;

    @Autowired
    public RESTAdminController(UserService userService) {
        this.userService = userService;
    }
}
