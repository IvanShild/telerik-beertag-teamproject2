package com.beertag.project.controllers.RESTControllers;
//
////TODO show top 3 beers
//
import com.beertag.project.models.User;
import com.beertag.project.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/users")
public class RESTUserController {
    private UserService userService;

    @Autowired
    public RESTUserController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping
    public List<User> getAll(@RequestParam(required = false) String username) {

        return userService.getAll();
    }


    @GetMapping("/{id}")
    public User getById(@PathVariable int id) {
        try {
            return userService.getById(id);
        } catch (IllegalArgumentException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PostMapping
    public User create(@Valid @RequestBody User user) {
        try {
            userService.create(user);
        } catch (IllegalArgumentException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
        return user;
    }

    @DeleteMapping("/{name}")
    public void delete(@PathVariable String name) {
        try {
            userService.delete(name);
        } catch (RuntimeException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

}
//@PutMapping("/edit")
//    public boolean editUser(@Valid @RequestBody User user) {
//        try {
//            return userService.editUser(user);
//        } catch (Exception ex) {
//
//        }
//        return true;
//    }
//
//    @PutMapping("/upload/avatar")
//    public boolean uploadAvatar() {
//        try {
//
//        } catch (Exception ex) {
//
//        }
//        return false;
//    }