package com.beertag.project.controllers.RESTControllers;
//TODO

import com.beertag.project.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/login")
public class RESTLoginController {
    private UserService userService;

    @Autowired
    public RESTLoginController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping
    public String showLoginPage() {
        return "peshoLogin";
    }

}
