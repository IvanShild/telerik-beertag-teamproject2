package com.beertag.project.controllers;

import com.beertag.project.models.User;
import com.beertag.project.models.UserRatingBeers;
import com.beertag.project.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;


@Controller
public class UserController {
    private UserService userService;

    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping("/myprofile")
    public String showMyProfile(Model model) {
        try {
            Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
            if (!(authentication instanceof AnonymousAuthenticationToken)) {
                User user = userService.getByName(authentication.getName());
                List<UserRatingBeers> listTopThree = userService.getTopThree(user);
                model.addAttribute("user", user);
                model.addAttribute("listTop", listTopThree);
                return "userMyProfile";
            }
            return "accessDenied";
        } catch (RuntimeException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND,
                    String.format("Problem occurred: %s", e.getMessage()) );
        }
    }

    @PostMapping("/uploadImage")
    public String uploadImage(@RequestParam("imageFile")MultipartFile imageFile, Model model) {
        System.out.println("sad");
        try {
            Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
            if (!(authentication instanceof AnonymousAuthenticationToken)) {
                System.out.println(authentication.getName());
                userService.updateAvatar(imageFile, authentication.getName());
                User user = userService.getByName(authentication.getName());
                List<UserRatingBeers> listTopThree = userService.getTopThree(user);
                model.addAttribute("user", user);
                model.addAttribute("listTop", listTopThree);
                return "index";
            }
            return "accessDenied";
        } catch (Exception e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND,
                    String.format("Problem occurred: %s", e.getMessage()) );
        }
    }

    @GetMapping("/users")
    public String getAllUsers(Model model) {
        try {
        List<User> users = userService.getAll();
        model.addAttribute("users", users);
        return "users";
        } catch (RuntimeException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND,
                    String.format("Problem occurred: %s", e.getMessage()) );
        }
    }

    @GetMapping("/users/{name}")
    public String getByName(Model model, @PathVariable String name) {
        try {
            User user = userService.getByName(name);

           List<UserRatingBeers> listTopThree = userService.getTopThree(user);


            model.addAttribute("user", user);
            model.addAttribute("listTop", listTopThree);
            return "userProfile";
        } catch (RuntimeException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND,
                    String.format("User with name %s does not exists.", name));
        }
    }

    @PutMapping("/users/{name}")
    public String update(Model model, @PathVariable String name, @RequestBody @Valid User user) {
        try {
            userService.update(name, user);
            model.addAttribute("userProfile", user);
            return "userProfile";
        } catch (RuntimeException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

    @DeleteMapping("/users/{name}")
    public String delete(@PathVariable String name) {
        try {
            userService.delete(name);
            return "users";
        } catch (RuntimeException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

//    @GetMapping("/users/{id}")
//    public User getById(@PathVariable int id) {
//        try {
//            return userService.getById(id);
//        } catch (RuntimeException e) {
//            throw new ResponseStatusException(HttpStatus.NOT_FOUND,
//                    String.format("User with id %d does not exists.", id));
//        }
//    }
//    @PutMapping("/users/{id}")
//    public User update(
//            @PathVariable int id,
//            @RequestBody @Valid User user) {
//        try {
//            userService.update(id, user);
//            return user;
//        } catch (RuntimeException e) {
//            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
//        }
//    }
//
//    @DeleteMapping("/users/{id}")
//    public void delete(@PathVariable int id) {
//        try {
//            userService.delete(id);
//        } catch (RuntimeException e) {
//            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
//        }
//    }
//

}

