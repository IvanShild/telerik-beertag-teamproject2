//package com.beertag.project.controllers;
//
//import com.beertag.project.services.ImageUploadService;
//import com.beertag.project.services.ImageUploadServiceImpl;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.http.HttpStatus;
//import org.springframework.stereotype.Controller;
//import org.springframework.web.bind.annotation.PostMapping;
//import org.springframework.web.bind.annotation.RequestParam;
//import org.springframework.web.multipart.MultipartFile;
//import org.springframework.web.server.ResponseStatusException;
//
//@Controller
//public class ImageUploadController {
//    private ImageUploadService service;
//
//    @Autowired
//    public ImageUploadController(ImageUploadServiceImpl service) {
//        this.service = service;
//    }
//
//    @PostMapping("/uploadImage")
//    public String uploadImage(@RequestParam("imageFile")MultipartFile imageFile) {
//        try {
//            service.saveImage(imageFile);
//            return "accessDenied";
//        } catch (Exception ex) {
//            throw new ResponseStatusException(HttpStatus.NOT_ACCEPTABLE, ex.getMessage());
//        }
//    }
//}
