package com.beertag.project.controllers;

import com.beertag.project.models.User;
import com.beertag.project.models.UserReg;
import com.beertag.project.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.provisioning.UserDetailsManager;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@Controller
public class RegistrationController {

    private UserService userService;

    private UserDetailsManager userDetailsManager;

    @Autowired
    public RegistrationController(UserDetailsManager userDetailsManager, UserService userService) {
        this.userDetailsManager = userDetailsManager;
        this.userService = userService;
    }

    @GetMapping("/register")
    public String showRegistrationPage(Model model) {
        model.addAttribute("userReg", new UserReg());
        return "register";
    }



    @PostMapping("/register")
    public String registerUser(@Valid @ModelAttribute UserReg userReg, BindingResult bindingResult, Model model) {
        if (bindingResult.hasErrors()) {
            model.addAttribute("error", "Username/password can't be empty!");
            return "register";
        }

        if(userDetailsManager.userExists(userReg.getUsername())) {
            model.addAttribute("error", "User with the same name already exists!");
            return "register";
        }

        List<GrantedAuthority> authorities = AuthorityUtils.createAuthorityList("ROLE_USER");
        org.springframework.security.core.userdetails.User newUser =
                new org.springframework.security.core.userdetails.User(
                        userReg.getUsername(),
                        "{noop}" + userReg.getPassword(),
                        authorities);
        userDetailsManager.createUser(newUser);

        User userDetails = new User();
        userDetails.setUsername(userReg.getUsername());
        userDetails.setPassword(userReg.getPassword());
        userDetails.setEmail(userReg.getEmail());
        userDetails.setPicture("");
        userService.create(userDetails);

        return "login";
    }
}
