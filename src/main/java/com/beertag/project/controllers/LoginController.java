//package com.beertag.project.controllers;
//
//import com.beertag.project.services.UserService;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Controller;
//import org.springframework.web.bind.annotation.GetMapping;
//
//@Controller
//public class LoginController {
//    private UserService userService;
//
//    @Autowired
//    public LoginController(UserService userService) {
//        this.userService = userService;
//    }
//
//    @GetMapping(("/login"))
//    public String showLoginPage() {
//        return "login";
//    }
//
//}
