package com.beertag.project.controllers;

import com.beertag.project.models.UserReg;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class HomeController {
    @GetMapping("/")
    public String showHomePage() {
        return "index";
    }

    @GetMapping("/accessdenied")
    public String showAccessDeniedPage() {
        return "accessDenied";
    }

//    @GetMapping("/register")
//    public String register(Model model) {
//        model.addAttribute("user", new UserReg());
//        return "register";
//    }

    @GetMapping("/about")
    public String about() {
        return "about";
    }

    @GetMapping("/beerslist")
    public String beersList() {
        return "beerslist";
    }

    @GetMapping("/contact")
    public String contact() {
        return "contact";
    }

    @GetMapping(("/login"))
    public String login() {
        return "login";
    }

    @GetMapping(("/access"))
    public String accessdenied() {
        return "accessDenied";
    }

    @GetMapping(("/cbeer"))
    public String createBeer() {
        return "beerCreate";
    }
}

