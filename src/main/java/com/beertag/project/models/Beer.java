package com.beertag.project.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.util.List;
import java.util.Set;


@Entity
@Table(name = "beers")
public class Beer {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "beer_id")
    private int id;

    //@NotBlank(message = "Beer name cannot be empty!")
    @Column(name = "beer_name", nullable = false)
    private String beerName;

    @Column(name = "beer_desc", nullable = false)
    private String description;

    @Column(name = "beer_abv", nullable = false)
    private double alcoholByVolume;

    @Column(name = "beer_avg_rating")
    private double averageRating;

    @Column(name = "beer_picture", nullable = false)
    private String picture;

    @ManyToOne
    @JoinColumn(name = "country_id")
    private CountryOfOrigin country;

    @Column(name = "brewery")
    private String brewery;

    @ManyToOne
    @JoinColumn(name = "style_id")
    private Style style;

    //(fetch = FetchType.EAGER,cascade = CascadeType.ALL)
    @LazyCollection(LazyCollectionOption.FALSE)
    @ManyToMany
//    @JsonIgnore
    @JoinTable(name = "beers_has_tags",
            joinColumns = @JoinColumn(name = "beer_id"),
            inverseJoinColumns = @JoinColumn(name = "tag_id"))
    private Set<Tag> tags;

    public Beer() {

    }

    public Beer(int id, String beerName,
                String description,
                double alcoholByVolume,
                double averageRating,
                String brewery,
                Style style) {
        this.id = 1;
        this.beerName = "";
        this.description = "";
        this.alcoholByVolume = 0.0;
        this.averageRating = 0.0;
//       this.tags = new ArrayList<>();
        this.brewery = "";
        this.style = new Style();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getBeerName() {
        return beerName;
    }

    public void setBeerName(String beerName) {
        this.beerName = beerName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public double getAlcoholByVolume() {
        return alcoholByVolume;
    }

    public void setAlcoholByVolume(double alcoholByVolume) {
        this.alcoholByVolume = alcoholByVolume;
    }

    public double getAverageRating() {
        return averageRating;
    }

    public void setAverageRating(double averageRating) {
        this.averageRating = averageRating;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public String getBrewery() {
        return brewery;
    }

    public void setBrewery(String brewery) {
        this.brewery = brewery;
    }

    public Style getStyle() {
        return style;
    }

    public void setStyle(Style style) {
        this.style = style;
    }

    public CountryOfOrigin getCountry() {
        return country;
    }

    public void setCountry(CountryOfOrigin country) {
        this.country = country;
    }

    public Set<Tag> getTags() {
        return tags;
    }

    public void setTags(Set<Tag> tags) {
        this.tags = tags;
    }
}



