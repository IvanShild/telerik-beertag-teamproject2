package com.beertag.project.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.Email;
import javax.validation.constraints.Size;

public class UserReg {

    @Size(min = 3, message = "Username should contain 3 or more characters!")
    private String username;

    @Size(min = 6, message = "Password should contain 6 or more characters!")
    private String password;

    @Email(message = "Email should be correct!")
    private String email;

    public UserReg() {
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
