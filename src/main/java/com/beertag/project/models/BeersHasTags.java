package com.beertag.project.models;

import javax.persistence.*;

@Entity
@Table(name = "beers_has_tags")
public class BeersHasTags {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "beer_has_tags_id")
    private int beersHasTagsId;

    @ManyToOne
    @JoinColumn(name = "beer_id")
    private Beer beer;

    @ManyToOne
    @JoinColumn(name = "tag_id")
    private Tag tag;

    public BeersHasTags(){

    }

    public int getBeersHasTagsId() {
        return beersHasTagsId;
    }

    public void setBeersHasTagsId(int beersHasTagsId) {
        this.beersHasTagsId = beersHasTagsId;
    }

    public Beer getBeer() {
        return beer;
    }

    public void setBeer(Beer beer) {
        this.beer = beer;
    }

    public Tag getTag() {
        return tag;
    }

    public void setTag(Tag tag) {
        this.tag = tag;
    }
}
