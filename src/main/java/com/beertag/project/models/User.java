package com.beertag.project.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import org.hibernate.annotations.*;
import org.hibernate.annotations.NamedQuery;

import javax.persistence.*;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.Email;
import javax.validation.constraints.Size;
import java.util.List;
import java.util.Set;

//@Where(clause="is_active=1")
//@SQLDelete(sql = "UPDATE user_details SET state = ‘DELETED’ WHERE id = ?", check = ResultCheckStyle.COUNT)

//@NamedQuery(name = "User.getName()", query = "SELECT u FROM User a WHERE user_name like :name")
@Entity
@Where(clause = "state = 0")
@Table(name = "user_details")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "user_id" /*, updatable = false, nullable = false */)
//    @GeneratedValue(strategy = GenerationType.IDENTITY)
//    @Column(name = "user_id")
    private int id;

    @Column(name = "user_name")
    private String username;

    @Column(name = "user_password")
    private String password;

    @Column(name = "user_email")
    private String email;

    @Column(name = "user_picture")
    private String picture;

//    @Column(name="is_active")
//    private Boolean active;

    @Column(name = "state")
    private Integer state = 0;

    @LazyCollection(LazyCollectionOption.FALSE)
    @ManyToMany
    @JsonIgnore
    @JoinTable(name = "users_drank_beers",
            joinColumns = @JoinColumn(name = "user_id"),
            inverseJoinColumns = @JoinColumn(name = "beer_id"))
    private Set<Beer> beersDrank;


    @LazyCollection(LazyCollectionOption.FALSE)
    @ManyToMany
    @JsonIgnore
    @JoinTable(name = "users_wish_beers",
            joinColumns = @JoinColumn(name = "user_id"),
            inverseJoinColumns = @JoinColumn(name = "beer_id"))
    private Set<Beer> beersWish;

//    @LazyCollection(LazyCollectionOption.FALSE)
//    @ManyToMany
//    @JsonIgnore
//    @JoinTable(name = "users_ratings_beers",
//            joinColumns = @JoinColumn(name = "beer_id"),
//            inverseJoinColumns = @JoinColumn(name = "user_id"))
//    private Set<UserRatingBeers> beersRated;


    public User() {

    }

    public User(int id, String username, String password, String email, String picture) {
        this.id = 1;
        this.username = "";
        this.password = "";
        this.email = "";
        this.picture = "";
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @JsonIgnore
    public Set<Beer> getBeersDrank() {
        return beersDrank;
    }

    public void setBeersDrank(Set<Beer> beersDrank) {
        this.beersDrank = beersDrank;
    }

    @JsonIgnore
    public Set<Beer> getBeersWish() {
        return beersWish;
    }

    public void setBeersWish(Set<Beer> beersWish) {
        this.beersWish = beersWish;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = 1;
    }

    //    public Set<UserRatingBeers> getBeersRated() {
//        return beersRated;
//    }
//
//    public void setBeersRated(Set<UserRatingBeers> beersRated) {
//        this.beersRated = beersRated;
//    }
}

