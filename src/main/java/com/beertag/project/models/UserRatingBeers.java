package com.beertag.project.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import java.util.List;
import java.util.Set;

@Entity
@Table(name = "users_ratings_beers")
public class UserRatingBeers {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "users_ratings_beers_id")
    private int UserRatingsId;

    @ManyToOne
    @JoinColumn(name = "beer_id")
    private Beer beer;

    @Column(name = "rating")
    private int rating;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;

//    @ManyToMany(mappedBy = "beersRated")
//    @JsonIgnoreProperties
//    private Set<User> users;


    public UserRatingBeers() {

    }

    public Beer getBeerId() {
        return beer;
    }

    public void setBeerId(Beer beer) {
        this.beer = beer;
    }

    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }

    public int getUserRatingsId() {
        return UserRatingsId;
    }

    public void setUserRatingsId(int userRatingsId) {
        UserRatingsId = userRatingsId;
    }

    public Beer getBeer() {
        return beer;
    }

    public void setBeer(Beer beer) {
        this.beer = beer;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
