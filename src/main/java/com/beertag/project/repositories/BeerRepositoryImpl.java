package com.beertag.project.repositories;

import com.beertag.project.models.*;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Repository
public class BeerRepositoryImpl implements BeerRepository {
    private SessionFactory sessionFactory;


    @Autowired
    public BeerRepositoryImpl(SessionFactory sessionFactory) {

        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<Beer> getAll() {
        Session session = sessionFactory.openSession();
        try {
            Query<Beer> query = session.createQuery("FROM Beer", Beer.class);
            return query.list();
        } finally {
            if (session != null) {
                session.close();
            }
        }
    }

    @Override
    public Beer getById(int id) {
        Session session = sessionFactory.openSession();
        try {
            return session.get(Beer.class, id);
        } finally {
            if (session != null) {
                session.close();
            }
        }
    }

    @Override
    public void create(Beer beer) {
//        try(Session session = sessionFactory.openSession()) {
//            session.save(beer);
//        }

        Session session = sessionFactory.openSession();
        try {
            session.save(beer);
        } finally {
            if (session != null) {
                session.close();
            }
        }
    }


    @Override
    public void update(int id, Beer beer) {
        Session session = sessionFactory.openSession();
        try {
            Beer newBeer = getById(id);
            newBeer.setBeerName(beer.getBeerName());
            newBeer.setAlcoholByVolume(beer.getAlcoholByVolume());
            newBeer.setBrewery(beer.getBrewery());
            newBeer.setDescription(beer.getDescription());
            newBeer.setPicture(beer.getPicture());
            newBeer.setStyle(beer.getStyle());

            Transaction transaction = session.beginTransaction();
            session.update(newBeer);
            transaction.commit();
        } finally {
            if (session != null) {
                session.close();
            }
        }
    }

    @Override
    public void delete(int id) {
        Session session = sessionFactory.openSession();
        try {
            Transaction transaction = session.beginTransaction();
            session.remove(getById(id));
            transaction.commit();
        } finally {
            if (session != null) {
                session.close();
            }
        }
    }

    public List<Beer> getByCountry(String name) {
        System.out.println("country");
        Session session = sessionFactory.openSession();
        try {
            Query<Beer> query = session.createQuery("SELECT beer_name AS Name,brewery_name AS brewery, country_name AS country " +
                    "FROM beers b JOIN breweries brew ON b.brewery_id=brew.brewery_id " +
                    "JOIN countries_of_origin cof ON brew.country_id = cof.country_id", Beer.class);
            query.setParameter("name", name);
            return query.list();
        } finally {
            if (session != null) {
                session.close();
            }
        }
    }

    public List<Beer> getByStyle(String name) {
        Session session = sessionFactory.openSession();
        try {
            System.out.println("style");
            Query<Beer> query = session.createQuery("FROM Style " +
                    "WHERE style_name=:name", Beer.class);
            query.setParameter("name", name);
            return query.list();
        } finally {
            if (session != null) {
                session.close();
            }
        }
    }

    @Override
    public Set<Beer> getByTag(List<String> tags) {
        Session session = sessionFactory.openSession();
        try {
            Set<Beer> getByTags = new HashSet<>();
            for (String tag : tags) {
                Query<Beer> query = session.createQuery(
                        "SELECT b FROM Beer b JOIN b.tags t" +
                                "WHERE t.name = :name", Beer.class);
                query.setParameter("name", tag);

                getByTags.addAll(query.list());
            }
            return getByTags;

        } finally {
            if (session != null) {
                session.close();
            }
        }
    }

    @Override
    public List<Beer> getBeersByStyle(int styleId) {
        System.out.println("StyleID");

        Session session = sessionFactory.openSession();
        try {
            Query<Beer> query = session.createQuery("FROM Beer WHERE style_id=:styleId", Beer.class);
            return query.list();
        } finally {
            if (session != null) {
                session.close();
            }
        }
    }

    @Override
    public List<Beer> getBeersByCountry(int countryId) {
        System.out.println("countryID");

        Session session = sessionFactory.openSession();
        try {
            Query<Beer> query = session.createQuery("FROM Beer WHERE country_id=:countryId", Beer.class);
            return query.list();
        } finally {
            if (session != null) {
                session.close();
            }
        }
    }

    @Override
    public List<Beer> getBeersByTag(int tagId) {
        System.out.println("tagId");

        Session session = sessionFactory.openSession();
        try {
            Query<BeersHasTags> query = session.createQuery("FROM BeersHasTags WHERE tag_id=:tagId", BeersHasTags.class);
            query.setParameter("tagId",tagId);
            List<BeersHasTags> test = query.list();
            List<Beer> test2 = new ArrayList<>();
            for (BeersHasTags beertags: test) {
                test2.add(beertags.getBeer());
            }
            System.out.println();
            return test2;
        } finally {
            if (session != null) {
                session.close();
            }
        }
    }

    @Override
    public CountryOfOrigin getCountryByName(String countryName) {
        Session session = sessionFactory.openSession();
        try {
            Query<CountryOfOrigin> query = session.createQuery("FROM CountryOfOrigin WHERE country_name=:countryName", CountryOfOrigin.class);
            query.setParameter("name", countryName);
            return query.getSingleResult();
        } finally {
            if (session != null) {
                session.close();
            }
        }
    }

    @Override
    public Tag getTagByName(String tag) {
        Session session = sessionFactory.openSession();
        try {
            Query<Tag> query = session.createQuery("FROM Tag WHERE tag_name=:tag", Tag.class);
            query.setParameter("tag", tag);
            System.out.println("asd");
            return query.getSingleResult();
        } finally {
            if (session != null) {
                session.close();
            }
        }
    }

    @Override
    public List<CountryOfOrigin> getAllCountries() {
        Session session = sessionFactory.openSession();
        try {
            Query<CountryOfOrigin> query = session.createQuery("FROM CountryOfOrigin", CountryOfOrigin.class);
            return query.list();
        } finally {
            if (session != null) {
                session.close();
            }
        }


    }

    @Override
    public List<Style> getAllStyles() {
        Session session = sessionFactory.openSession();
        try {
            Query<Style> query = session.createQuery("FROM Style", Style.class);
            return query.list();
        } finally {
            if (session != null) {
                session.close();
            }
        }
    }

}

