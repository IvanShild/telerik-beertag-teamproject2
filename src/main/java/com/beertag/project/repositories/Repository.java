package com.beertag.project.repositories;

import java.util.List;

public interface Repository<T> {

    void create(T entity);

    List<T> getAll();

    T getById(int id);

    void update(int id, T entity);

    void delete(int id);
}
