package com.beertag.project.repositories;

import com.beertag.project.models.User;
import com.beertag.project.models.UserRatingBeers;
import com.beertag.project.models.UserReg;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface UserRepository {

    void create(User user);

    List<User> getAll();

    User getById(int id);

    void update(User newUser, UserReg newUseReg);

    void delete(String name);

    User getByUserName(String username);

    UserReg getByUserNameSecurity(String name);

    List<UserRatingBeers> getUserRatings(int id);
}
