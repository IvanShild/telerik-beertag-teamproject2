package com.beertag.project.repositories;

import com.beertag.project.models.Beer;
import com.beertag.project.models.User;
import com.beertag.project.models.UserRatingBeers;

import java.util.List;

public interface UserRatingBeersRepository {

    void createRating(User user, Beer beer, int rating);

    UserRatingBeers getByUserBeer(int userId, int beerId);

    void updateRating(UserRatingBeers userRatingBeers);

    UserRatingBeers getByIds(int userId, int beerId);

    List<Beer> getTopThreeUserRatedBeers(int userId);
}
