package com.beertag.project.repositories;

import com.beertag.project.models.User;
import com.beertag.project.models.UserRatingBeers;
import com.beertag.project.models.UserReg;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class UserRepositoryImpl implements UserRepository {
    private SessionFactory sessionFactory;

    @Autowired
    public UserRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }


    @Override
    public List<User> getAll() {
        Session session = sessionFactory.openSession();
        try {
            Query<User> query = session.createQuery("FROM User", User.class);
            return query.list();
        } finally {
            if (session != null) {
                session.close();
            }
        }
    }

    @Override
    public User getById(int id) {
        Session session = sessionFactory.openSession();
        try {
            return session.get(User.class, id);
        } finally {
            if (session != null) {
                session.close();
            }
        }
    }

    @Override
    public void create(User user) {
        Session session = sessionFactory.openSession();
        try {
            session.save(user);
        } finally {
            if (session != null) {
                session.close();
            }
        }

    }

    public UserReg getByUserNameSecurity(String name) {
        Session session = sessionFactory.openSession();
        try {
            Query<UserReg> query = session.createQuery("from UserReg where username=:name", UserReg.class);
            query.setParameter("name", name);
            return query.getSingleResult();
        } finally {
            if (session != null) {
                session.close();
            }
        }
    }

    @Override
    public List<UserRatingBeers> getUserRatings(int id) {
        Session session = sessionFactory.openSession();
        try {
            Query<UserRatingBeers> query = session.createQuery("FROM UserRatingBeers WHERE user_id=:id", UserRatingBeers.class);
            query.setParameter("id",id);
            return query.list();
        } finally {
            if (session != null) {
                session.close();
            }
        }
    }


    public User getByUserName(String name) {
        Session session = sessionFactory.openSession();
        try {
            Query<User> query = session.createQuery("from User where user_name=:name", User.class);
            query.setParameter("name", name);
            return query.getSingleResult();
        } finally {
            if (session != null) {
                session.close();
            }
        }
    }

    @Override
    public void update(User newUser, UserReg newUserReg) {
        Session session = sessionFactory.openSession();
        try {
            Transaction transaction = session.beginTransaction();
            session.update(newUser);
            session.update(newUserReg);
            transaction.commit();
        } finally {
            if (session != null) {
                session.close();
            }
        }

    }

    @Override
    public void delete(String name) {
        Session session = sessionFactory.openSession();
        try {
            Transaction transaction = session.beginTransaction();
            session.remove(getByUserName(name));
            session.remove(getByUserNameSecurity(name));
            transaction.commit();
        } finally {
            if (session != null) {
                session.close();
            }
        }
    }


    //TODO
    public void saveAvatarAdress() {
        Session session = sessionFactory.openSession();
        try {
            Transaction transaction = session.beginTransaction();
            transaction.commit();
        } finally {
            if (session != null) {
                session.close();
            }
        }
    }
}
