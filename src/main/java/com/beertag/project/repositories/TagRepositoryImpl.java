package com.beertag.project.repositories;

import com.beertag.project.models.Beer;
import com.beertag.project.models.Tag;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Repository
public class TagRepositoryImpl implements TagRepository {
    private SessionFactory sessionFactory;

    @Autowired
    public TagRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    public Set<Beer> filterBeersByTag(List<String> tags) {
        Session session = sessionFactory.getCurrentSession();
        Query<Beer> query = session.createQuery("select b from Beer  b join b.tags t where t.name in (:tags) group by t.name ", Beer.class);
        query.setParameterList("tags", tags);
        Set<Beer> filteredByTags = new HashSet<>(query.list());
        return filteredByTags;

    }

    @Override
    public String addTag(Tag tag, int beerID) {
        Session session = sessionFactory.openSession();
        try {
            Transaction transaction = session.beginTransaction();
            Beer beer = session.get(Beer.class, beerID);
            beer.getTags().add(tag);
            session.save(beer);
            transaction.commit();
        } finally {
            if (session != null) {
                session.close();
            }
        }
        return tag.getTagName();
    }

    @Override
    public List<Tag> getAll() {
        Session session = sessionFactory.openSession();
        try {
            Query<Tag> query = session.createQuery("FROM Tag", Tag.class);
            return query.list();
        } finally {
            if (session != null) {
                session.close();
            }
        }
    }

    @Override
    public Tag getById(int id) {
        Session session = sessionFactory.openSession();
        try {
            return session.get(Tag.class, id);
        } finally {
            if (session != null) {
                session.close();
            }
        }
    }

    public void create(Tag tag) {
        Session session = sessionFactory.openSession();
        try {
            session.save(tag);
        } finally {
            if (session != null) {
                session.close();
            }
        }
    }

    public void removeTagFromBeer(Tag tag, int beerID) {
        Session session = sessionFactory.openSession();
        try {
            Transaction transaction = session.beginTransaction();
            Beer beer = session.get(Beer.class, beerID);
            beer.getTags().add(tag);
            session.remove(beer);
            transaction.commit();
        } finally {
            if (session != null) {
                session.close();
            }
        }
    }

    //remove, getAll(), getByID()
}
