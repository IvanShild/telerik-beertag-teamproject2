package com.beertag.project.repositories;

import com.beertag.project.models.Style;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class StyleRepositoryImpl implements StyleRepository {
    private SessionFactory sessionFactory;

    @Autowired
    public StyleRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<Style> getAll() {
        Session session = sessionFactory.openSession();
        try {
            return session
                    .createQuery("FROM Style", Style.class)
                    .list();
        } finally {
            if (session != null) {
                session.close();
            }
        }

    }

    @Override
    public Style getById(int id) {
        Session session = sessionFactory.openSession();
        try {
            return session.get(Style.class, id);
        } finally {
            if (session != null) {
                session.close();
            }
        }

    }

    @Override
    public Style getByName(String beerStyle) {
        Session session = sessionFactory.openSession();
        try {
            Query<Style> query = session.createQuery("FROM Style WHERE style_name = :name",Style.class);
            query.setParameter("name",beerStyle);
            return query.getSingleResult();
        } finally {
            if (session != null) {
                session.close();
            }
        }
    }
}

