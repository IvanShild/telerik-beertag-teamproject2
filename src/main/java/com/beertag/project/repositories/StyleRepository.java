package com.beertag.project.repositories;

import com.beertag.project.models.Style;

import java.util.List;

public interface StyleRepository {
    List<Style> getAll();

    Style getById(int id);

    Style getByName(String beerStyle);
}
