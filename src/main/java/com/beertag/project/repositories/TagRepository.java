package com.beertag.project.repositories;

import com.beertag.project.models.Beer;
import com.beertag.project.models.Tag;

import java.util.List;
import java.util.Set;

public interface TagRepository {
    List<Tag> getAll();

    void create(Tag tag);

    Tag getById(int id);

    String addTag(Tag tag, int beerID);

    void removeTagFromBeer(Tag tag, int beerID);

    Set<Beer> filterBeersByTag(List<String> tags);

}
