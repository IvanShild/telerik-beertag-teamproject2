package com.beertag.project.repositories;

import com.beertag.project.models.Beer;
import com.beertag.project.models.User;
import com.beertag.project.models.UserRatingBeers;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class UserRatingBeersRepositoryImpl implements UserRatingBeersRepository {
    private SessionFactory sessionFactory;

    @Autowired
    public UserRatingBeersRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    public UserRatingBeers getByIds(int userId, int beerId) {
        UserRatingBeers userVote = null;
        Session session = sessionFactory.openSession();
        try {
            Query<UserRatingBeers> query = session.createQuery("FROM UsersRatingsBeers AS urb" +
                    "WHERE urb.user_id = :user_id AND urb.beer_id = :beer_Id", UserRatingBeers.class)
                    .setParameter("beer_id", beerId);
            userVote = query.setMaxResults(1).uniqueResult();
            return userVote;
        } finally {
            if (session != null) {
                session.close();
            }
        }
    }


    public List<Beer> getTopThreeUserRatedBeers(int userId) {
        Session session = sessionFactory.openSession();
        try {
            Query<Beer> topThreeBeers = session.createQuery("FROM UsersRatingsBeers AS urb" +
                    " WHERE urb.user_id = :user_id, urb.beer_id = :beer_id" +
                    " ORDER BY urb.rating", Beer.class)
                    .setParameter("userId", userId)
                    .setMaxResults(3);
            return topThreeBeers.list();
        } finally {
            if (session != null) {
                session.close();
            }
        }
    }

    public void createRating(User user, Beer beer, int rating) {
        Session session = sessionFactory.openSession();
        try {
            UserRatingBeers userRatingBeers = new UserRatingBeers();
            userRatingBeers.setUser(user);
            userRatingBeers.setBeer(beer);
            userRatingBeers.setRating(rating);

            session.save(userRatingBeers);
        } finally {
            if (session != null) {
                session.close();
            }
        }
    }

    @Override
    public UserRatingBeers getByUserBeer(int userId, int beerId) {
        Session session = sessionFactory.openSession();
        try {
            Query<UserRatingBeers> query = session.createQuery("FROM UserRatingBeers AS urb " +
                    "WHERE urb.user_id = :userId AND urb.beer_id = :beerId ", UserRatingBeers.class)
                    .setParameter("userId", userId);

            return query.getSingleResult();

        } finally {
            if (session != null) {
                session.close();
            }
        }
    }

    @Override
    public void updateRating(UserRatingBeers userRatingBeers) {
        Session session = sessionFactory.openSession();
        try {
            Transaction transaction = session.beginTransaction();
            session.update(userRatingBeers);
            transaction.commit();

        } finally {
            if (session != null) {
                session.close();
            }
        }
    }
}
