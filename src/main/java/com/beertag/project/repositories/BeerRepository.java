package com.beertag.project.repositories;

import com.beertag.project.models.Beer;
import com.beertag.project.models.CountryOfOrigin;
import com.beertag.project.models.Style;
import com.beertag.project.models.Tag;

import java.util.List;
import java.util.Set;

public interface BeerRepository {

    void create(Beer beer);

    List<Beer> getAll();

    Beer getById(int id);

    void update(int id, Beer beer);

    void delete(int id);

    List<Beer> getByCountry(String countryName);

    List<Beer> getByStyle(String styleName);

    Set<Beer> getByTag(List<String> tags);

    List<CountryOfOrigin> getAllCountries();

    List<Style> getAllStyles();

    List<Beer> getBeersByStyle(int styleId);

    List<Beer> getBeersByCountry(int countryId);

    List<Beer> getBeersByTag(int tagId);

    CountryOfOrigin getCountryByName(String  country);

    Tag getTagByName(String tag);


}
