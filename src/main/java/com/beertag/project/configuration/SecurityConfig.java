package com.beertag.project.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpMethod;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.provisioning.JdbcUserDetailsManager;
import org.springframework.security.provisioning.UserDetailsManager;

import javax.sql.DataSource;

@Configuration
@EnableWebSecurity
@PropertySource("application.properties")
public class SecurityConfig extends WebSecurityConfigurerAdapter {
    private String dbUrl, dbUsername, dbPassword;

    public SecurityConfig(Environment env) {
        dbUrl = env.getProperty("database.url");
        dbUsername = env.getProperty("database.username");
        dbPassword = env.getProperty("database.password");
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.jdbcAuthentication()
                .dataSource(securityDataSource());
//        auth.inMemoryAuthentication()
//                .withUser(User.withUsername("penyo").password("{noop}pass1").roles("USER", "ADMIN"))
//                .withUser(User.withUsername("ivan").password("{noop}pass2").roles("USER"))
//                .withUser(User.withUsername("pesho").password("{noop}pass3").roles("USER"));
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests()
//                .antMatchers("/", "/register", "/login", "/css/**", "/images/**", "/api/**").permitAll()
//                .antMatchers("/admin/**").hasRole("ADMIN")
//                .antMatchers("/**").hasAnyRole("ADMIN", "USER")
//                .and()
//                .formLogin()
//                .and()
//                .logout()
//                .logoutSuccessUrl("/login")
//                .permitAll()
//                .and()
//                .csrf()
//                .disable();

//
//               .antMatchers(HttpMethod.DELETE, new String [] {"/", "/register", "/login", "/css/**", "/images/**", "/api/**"}).permitAll()
                .antMatchers("/", "/register", "/login", "/beers", "/beers/**", "/beers/**", "/css/**", "/avatars/**", "/js/**", "/images/**", "/api/**", "/api/**/**", "/resources/**/**/**", "/fonts/**").permitAll()
                .anyRequest()
                .authenticated()
//                .antMatchers("/").hasRole("USER")
                .antMatchers("/admin").hasRole("ADMIN")
                .and()
                .formLogin()
                .loginPage("/login")
                .loginProcessingUrl("/authenticate")
                .permitAll()
                .and()
                .logout()
                .permitAll()
                .and()
                .exceptionHandling().accessDeniedPage("/access-denied");

//                .antMatchers("/", "/register", "/login", "/css/**", "/images/**", "/api/**").permitAll()
//                .anyRequest()
//                .authenticated()
//                .antMatchers("/admin", "/admin/**").hasRole("ADMIN")
//                .and()
//                .formLogin()
//                .loginPage("/login")
//                .loginProcessingUrl("/authenticate")
//                .permitAll()
//                .and()
//                .logout()
//                .permitAll()
//                .and()
//                .exceptionHandling().accessDeniedPage("/access-denied");
    }

    @Bean
    public DataSource securityDataSource() {
        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName("com.mysql.cj.jdbc.Driver");
        dataSource.setUrl(dbUrl);
        dataSource.setUsername(dbUsername);
        dataSource.setPassword(dbPassword);

        return dataSource;
    }

    @Bean
    public UserDetailsManager userDetailsManager() {
        JdbcUserDetailsManager jdbcUserDetailsManager = new JdbcUserDetailsManager();
        jdbcUserDetailsManager.setDataSource(securityDataSource());
        return jdbcUserDetailsManager;
    }


}
