package com.beertag.project.services;

import com.beertag.project.models.Beer;
import com.beertag.project.models.Tag;

import java.util.List;
import java.util.Set;

public interface TagService {

    String addTag(Tag tag, int beerID);

    List<Tag> getAll();

    void create(Tag tag);

    Tag getById(int id);

    void removeTagFromBeer(Tag tag, int beerID);

    Set<Beer> filterBeersByTag(List<String> tags);
}
