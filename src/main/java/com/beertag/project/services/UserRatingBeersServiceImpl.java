package com.beertag.project.services;

import com.beertag.project.models.Beer;
import com.beertag.project.models.User;
import com.beertag.project.models.UserRatingBeers;
import com.beertag.project.repositories.UserRatingBeersRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserRatingBeersServiceImpl implements UserRatingBeersService {
    private UserRatingBeersRepository userRatingBeersRepository;
    private UserService userService;
    private BeerService beerService;

    @Autowired
    public UserRatingBeersServiceImpl(UserRatingBeersRepository userRatingBeersRepository, UserService userService, BeerService beerService) {
        this.userRatingBeersRepository = userRatingBeersRepository;
        this.userService = userService;
        this.beerService = beerService;
    }


    @Override
    public void createRating(int userId, int beerId, int rating) {
        User user = userService.getById(userId);
        Beer beer = beerService.getById(beerId);
        userRatingBeersRepository.createRating(user,beer,rating);
    }

    @Override
    public void updateRating(int userId, int beerId, int rating) {
        UserRatingBeers userRatingBeers = userRatingBeersRepository.getByUserBeer(userId, beerId);
        userRatingBeers.setRating(rating);
        userRatingBeersRepository.updateRating(userRatingBeers);
    }

    @Override
    public UserRatingBeers getByIds(int userId, int beerId) {
        return userRatingBeersRepository.getByIds(userId, beerId);
    }

    @Override
    public List<Beer> getTopThreeUserRatedBeers(int userId) {
        return userRatingBeersRepository.getTopThreeUserRatedBeers(userId);
    }
}
