package com.beertag.project.services;

import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

@Service
public class ImageUploadServiceImpl implements ImageUploadService {

    public void saveImage(MultipartFile imageFile) throws Exception {
        String folder = "C://Users//ASUS//IdeaProjects//BEER Tag//telerik-beertag-teamproject2//src//main//resources//static//avatars//";
        byte[] bytes = imageFile.getBytes();
        Path path = Paths.get(folder + imageFile.getOriginalFilename());
        Files.write(path, bytes);
    }
}
