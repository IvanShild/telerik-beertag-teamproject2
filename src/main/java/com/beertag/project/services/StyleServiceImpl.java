package com.beertag.project.services;

import com.beertag.project.models.Style;
import com.beertag.project.repositories.StyleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class StyleServiceImpl implements StyleService {
    private StyleRepository styleRepository;

    @Autowired
    public StyleServiceImpl(StyleRepository styleRepository) {

        this.styleRepository = styleRepository;
    }

    @Override
    public List<Style> getAll() {
        return styleRepository.getAll();
    }

    @Override
    public Style getById(int id) {
        return styleRepository.getById(id);
    }
}
