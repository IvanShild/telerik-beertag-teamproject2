package com.beertag.project.services;

import com.beertag.project.models.Beer;
import com.beertag.project.models.CountryOfOrigin;
import com.beertag.project.models.Style;
import com.beertag.project.models.Tag;
import com.beertag.project.repositories.BeerRepository;
import com.beertag.project.repositories.BeerRepositoryImpl;
import com.beertag.project.repositories.StyleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Comparator;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class BeerServiceImpl implements BeerService {
    private BeerRepository beerRepository;
    private StyleRepository styleRepository;

    @Autowired
    public BeerServiceImpl(BeerRepository beerRepository,StyleRepository styleRepository) {

        this.beerRepository = beerRepository;
        this.styleRepository = styleRepository;
    }

    @Override
    public List<Beer> getAll() {
        return beerRepository.getAll();
    }

    @Override
    public Beer getById(int id) {
        Beer beer = beerRepository.getById(id);
        if (beer == null) {
            throw new IllegalArgumentException(
                    String.format("Beer with id %d not found.", id));
        }

        return beerRepository.getById(id);
    }

    @Override
    public void create(Beer beer) {
        List<Beer> beers = beerRepository.getAll().stream()
                .filter(beer1 -> beer1.getBeerName().equals(beer.getBeerName()))
                .collect(Collectors.toList());

        if (beers.size() > 0) {
            throw new IllegalArgumentException(
                    String.format("Beer with name %s already exists", beer.getBeerName()));
        }
        beer.setPicture("../avatars/defaultBeer.jpg");

        beerRepository.create(beer);
    }


    @Override
    public void delete(int id) {
        Beer beer = beerRepository.getById(id);

        beerRepository.delete(id);
    }

    @Override
    public List<Beer> getByCountry(String countryName) {
        return beerRepository.getByCountry(countryName);
    }

    @Override
    public List<Beer> getByStyle(String styleName) {
        return beerRepository.getByStyle(styleName);
    }

    @Override
    public Set<Beer> getByTag(List<String> tags) {
        return beerRepository.getByTag(tags);
    }


    @Override
    public void update(int id, Beer beer) {
        beerRepository.update(id, beer);
    }


    @Override
    public List<Beer> sortByName() {
        List<Beer> beers = beerRepository.getAll();
        beers.sort(Comparator.comparing(Beer::getBeerName));
        return beers;
    }

    @Override
    public List<Beer> sortByABV() {
        List<Beer> beers = beerRepository.getAll();
        beers.sort(Comparator.comparing(Beer::getAlcoholByVolume));
        return beers;
    }

    @Override
    public List<Beer> sortByAverageRating() {
        List<Beer> beers = beerRepository.getAll();
        beers.sort(Comparator.comparing(Beer::getAverageRating));
        return beers;
    }

    @Override
    public List<CountryOfOrigin> getAllCountries() {
        return beerRepository.getAllCountries();
    }

    @Override
    public List<Style> getAllStyles() {
        return beerRepository.getAllStyles();
    }

    @Override
    public List<Beer> getBeersByStyle(String beerStyle) {
        Style newStyle = styleRepository.getByName(beerStyle);
        int styleId = newStyle.getStyleId();
        return beerRepository.getBeersByStyle(styleId);
    }

    @Override
    public List<Beer> getBeersByCountry(String countryOfOrigin) {
        CountryOfOrigin newCountry = beerRepository.getCountryByName(countryOfOrigin);
        int countryId = newCountry.getCountryId();
        return beerRepository.getBeersByCountry(countryId);
    }

    @Override
    public List<Beer> getBeersByTags(String tag) {
        Tag newTag = beerRepository.getTagByName(tag);
        int tagId = newTag.getTagId();
        return beerRepository.getBeersByTag(tagId);
    }


}
