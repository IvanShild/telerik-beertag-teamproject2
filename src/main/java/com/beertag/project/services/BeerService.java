package com.beertag.project.services;

import com.beertag.project.models.Beer;
import com.beertag.project.models.CountryOfOrigin;
import com.beertag.project.models.Style;
import com.beertag.project.models.Tag;
import org.springframework.data.domain.Sort;

import java.util.List;
import java.util.Set;

public interface BeerService {

    void create(Beer beer);

    List<Beer> getAll();

    Beer getById(int id);

    void update(int id, Beer beer);

    void delete(int id);

    List<Beer> getByCountry(String countryName);

    List<Beer> getByStyle(String styleName);

    Set<Beer> getByTag(List<String> tags);

    List<Beer> sortByName();

    List<Beer> sortByABV();

    List<Beer> sortByAverageRating();

    List<CountryOfOrigin> getAllCountries();

    List<Style> getAllStyles();

    List<Beer> getBeersByStyle(String beerStyle);

    List<Beer> getBeersByCountry(String countryOfOrigin);

    List<Beer> getBeersByTags(String tag);


//    void addEmployee(int id, Employee employee);
//
//    void removeEmployee(int id, int employeeId);
//
//    void delete(int id);

}
