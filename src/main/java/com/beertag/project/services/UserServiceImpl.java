package com.beertag.project.services;

import com.beertag.project.models.User;
import com.beertag.project.models.UserRatingBeers;
import com.beertag.project.models.UserReg;
import com.beertag.project.repositories.Repository;
import com.beertag.project.repositories.UserRepository;
import com.beertag.project.repositories.UserRepositoryImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class UserServiceImpl implements UserService {

    private UserRepository userRepository;

    @Autowired
    public UserServiceImpl(UserRepository userRepository) {

        this.userRepository = userRepository;
    }


    @Override
    public List<User> getAll() {
        return userRepository.getAll();
    }


    @Override
    public User getById(int id) {
        User user = userRepository.getById(id);
        if (user == null) {
            throw new IllegalArgumentException(
                    String.format("User with id %d not found.", id));
        }
        return user;
    }

    @Override
    public User getByName(String name) {
        return userRepository.getByUserName(name);
    }

    @Override
    public void delete(String name) {
        userRepository.delete(name);
    }


    @Override
    public void create(User user) {
        List<User> users = userRepository.getAll().stream()
                .filter(user1 -> user1.getUsername().equals(user.getUsername()))
                .collect(Collectors.toList());

        if (users.size() > 0) {
            throw new IllegalArgumentException(
                    String.format("User with username %s already exists", user.getUsername()));
        }

        userRepository.create(user);
    }

    @Override
    public void update(String name, User user) {
        User newUser = userRepository.getByUserName(name);
        newUser.setUsername(user.getUsername());
        newUser.setPassword(user.getPassword());
        newUser.setEmail(user.getEmail());
        newUser.setPicture(user.getPicture());

        UserReg newUserReg = userRepository.getByUserNameSecurity(name);
        newUserReg.setUsername(user.getUsername());
        newUserReg.setPassword(user.getPassword());

        if (name == user.getUsername()) {
            userRepository.update(newUser, newUserReg);
        } else {
            List<User> users = userRepository.getAll().stream()
                    .filter(user1 -> user1.getUsername().equals(user.getUsername()))
                    .collect(Collectors.toList());

            if (users.size() > 0) {
                throw new IllegalArgumentException(
                        String.format("User with username %s already exists", user.getUsername()));
            }

            userRepository.update(newUser, newUserReg);
        }
    }

    @Override
    public List<UserRatingBeers> getTopThree(User user) {
        int id = user.getId();
        List<UserRatingBeers> getTop = userRepository.getUserRatings(id);
        getTop.sort(Comparator.comparing(UserRatingBeers::getRating));
        List<UserRatingBeers> topThree = new ArrayList<>();
        for (int i = 0; i < getTop.size(); i++) {
            topThree.add(getTop.get(i));
        }

        return topThree;
    }

    @Override
    public void updateAvatar(MultipartFile imageFile, String user) throws  Exception{
        String folder = "C://Users//secretpc72//Desktop//BeerTag TeamProject//telerik-beertag-teamproject2//src//main//resources//static//avatars//";
        byte[] bytes = imageFile.getBytes();
        Path path = Paths.get(folder + user);
        Files.write(path, bytes);
        User updatUser = userRepository.getByUserName(user);
        UserReg userReg = userRepository.getByUserNameSecurity(user);
        updatUser.setPicture(String.format("../avatars/%s.%s", user, imageFile.getOriginalFilename().split(".")[1]));
        userRepository.update(updatUser, userReg);
    }

}
