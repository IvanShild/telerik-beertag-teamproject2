package com.beertag.project.services;

import com.beertag.project.models.Beer;
import com.beertag.project.models.UserRatingBeers;

import java.util.List;

public interface UserRatingBeersService {

    void createRating(int userId, int beerId, int rating);

    void updateRating(int userId, int beerId, int rating);

    UserRatingBeers getByIds(int userId, int beerId);

    List<Beer> getTopThreeUserRatedBeers(int userId);
}
