package com.beertag.project.services;

import com.beertag.project.models.User;
import com.beertag.project.models.UserRatingBeers;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

public interface UserService {

    List<User> getAll();

    User getById(int id);

    User getByName(String name);

    void delete(String name);

    void create(User user);

    void update(String name, User user);

    List<UserRatingBeers> getTopThree(User user);

    void updateAvatar(MultipartFile imageFile, String user) throws Exception;
}
