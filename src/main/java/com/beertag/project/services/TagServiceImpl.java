package com.beertag.project.services;

import com.beertag.project.models.Beer;
import com.beertag.project.models.Tag;
import com.beertag.project.repositories.TagRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class TagServiceImpl implements TagService {

    private TagRepository tagRepository;

    @Autowired
    public TagServiceImpl(TagRepository tagRepository) {
        this.tagRepository = tagRepository;
    }

    @Override
    public String addTag(Tag tag, int beerID) {
        String name = tag.getTagName();

        List<Tag> TagAlreadyExist =
                tagRepository.getAll()
                        .stream()
                        .filter(u -> u.getTagName().equals(name))
                        .collect(Collectors.toList());

        if (!TagAlreadyExist.isEmpty()) {
            tagRepository.addTag(TagAlreadyExist.get(0), beerID);
        }
        tagRepository.create(tag);
        tagRepository.addTag(tag, beerID);
        return "Tag was successfully added!";
    }

    @Override
    public List<Tag> getAll() {
        return tagRepository.getAll();
    }

    @Override
    public void create(Tag tag) {
        List<Tag> tags = tagRepository.getAll().stream()
                .filter(tag1 -> tag1.getTagName().equals(tag.getTagName()))
                .collect(Collectors.toList());

        if (tags.size() > 0) {
            throw new IllegalArgumentException(
                    String.format("Tag with name %s already exists", tag.getTagName()));
        }
        tagRepository.create(tag);
    }

    @Override
    public Tag getById(int id) {
        Tag tag = tagRepository.getById(id);
        if (tag == null) {
            throw new IllegalArgumentException(String.format("Tag with id %d not found", id));
        }
        return tagRepository.getById(id);
    }

    @Override
    public void removeTagFromBeer(Tag tag, int beerID) {
        if (tag == null) {
            throw new IllegalArgumentException("Beer doesn't have this tag");
        }
        tagRepository.removeTagFromBeer(tag, beerID);
    }

    @Override
    public Set<Beer> filterBeersByTag(List<String> tags) {
        return tagRepository.filterBeersByTag(tags);
    }

}
