package com.beertag.project.services;

import com.beertag.project.models.Style;

import java.util.List;

public interface StyleService {
    List<Style> getAll();

    Style getById(int id);

}
