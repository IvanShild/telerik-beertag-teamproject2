package com.beertag.project.services;

import org.springframework.web.multipart.MultipartFile;

public interface ImageUploadService {
    void  saveImage(MultipartFile imageFile) throws Exception;
}
