package com.beertag.project.mockobjects;

import com.beertag.project.models.Beer;
import com.beertag.project.models.Style;
import com.beertag.project.repositories.BeerRepository;
import com.beertag.project.services.BeerServiceImpl;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.Arrays;

@RunWith(MockitoJUnitRunner.class)
public class BeerServiceImplTests {

    @Mock
    private BeerRepository beerMockRepository;

    @InjectMocks
    private BeerServiceImpl beerService;

    @Test
    public void getById_Should_ReturnBeer_When_BeerWithSameIdExists() {
        // Arrange
        Mockito.when(beerMockRepository.getById(1))
                .thenReturn(new Beer(1, "Zagorka", "", 5.0, 9.0, "", new Style()));

        // Act
        Beer result = beerService.getById(1);

        // Assert
        Assert.assertEquals(1, result.getId());
    }

    @Test(expected = IllegalArgumentException.class)
    public void getById_Should_ThrowException_When_BeerNotExists() {
        // Arrange
        Mockito.when(beerMockRepository.getById(1))
                .thenReturn(null);

        // Act
        beerService.getById(1);
    }

    @Test
    public void create_Should_CallRepositoryCreate_When_BeerNameIsUnique() {
        // Arrange
        Beer newBeer = new Beer(1, "Zagorka", "", 5.0, 9.0, "", new Style());

        // Act
        beerService.create(newBeer);

        // Assert
        Mockito.verify(beerMockRepository, Mockito.times(1)).create(newBeer);
    }

    @Test(expected = IllegalArgumentException.class)
    public void create_Should_ThrowException_When_BeerNameIsNotUnique() {
        // Arrange
        Beer newBeer = new Beer(1, "Zagorka", "", 5.0, 9.0, "", new Style());
        Mockito.when(beerMockRepository.getAll())
                .thenReturn(Arrays.asList(
                        new Beer(1, "Zagorka Retro", "", 5.2, 9.0, "", new Style()),
                        new Beer(2, "Zagorka", "", 5.0, 9.0, "", new Style()),
                        new Beer(3, "Zagorka Rezerva", "", 7.0, 9.0, "", new Style())
                ));


        // Act
        beerService.create(newBeer);

        // Assert
        Mockito.verify(beerMockRepository, Mockito.never()).create(newBeer);
    }

    @Test
    public void delete_Should_CallRepositoryDelete_When_RepoHasBeerId() {
        // Arrange
        Beer newBeer = new Beer(1, "Zagorka", "", 5.0, 9.0, "", new Style());

        // Act
        beerService.delete(1);

        // Assert
        Mockito.verify(beerMockRepository, Mockito.times(1)).delete(1);
    }

    @Test(expected = IllegalArgumentException.class)
    public void delete_Should_ThrowException_When_RepoDoesntHaveBeerId() {
        // Arrange
        Beer newBeer = new Beer(100, "Zagorka", "", 5.0, 9.0, "", new Style());

        Mockito.when(beerMockRepository.getById(100))
                .thenReturn(null);

        // Act
        beerService.getById(100);
    }
}



