package com.beertag.project.mockobjects;

import com.beertag.project.models.User;
import com.beertag.project.repositories.Repository;
import com.beertag.project.repositories.UserRepository;
import com.beertag.project.services.UserServiceImpl;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Arrays;

@RunWith(MockitoJUnitRunner.class)
public class UserServiceImplTests {

    @Mock
    private UserRepository userMockRepository;

    @InjectMocks
    private UserServiceImpl userService;

    @Test
    public void getById_Should_ReturnUser_When_UserWithSameIdExists() {
        // Arrange
        Mockito.when(userMockRepository.getById(1))
                .thenReturn(new User(1, "petko", "1234567", "petko@abv.bg", ""));

        // Act
        User result = userService.getById(1);

        // Assert
        Assert.assertEquals(1, result.getId());
    }

    @Test(expected = IllegalArgumentException.class)
    public void getById_Should_ThrowException_When_UserNotExists() {
        // Arrange
        Mockito.when(userMockRepository.getById(1))
                .thenReturn(null);

        // Act
        userService.getById(1);
    }

    @Test
    public void create_Should_CallRepositoryCreate_When_UsernameIsUnique() {
        // Arrange
        User newUser = new User(1, "petko", "1234567", "petko@abv.bg", "");

        // Act
        userService.create(newUser);

        // Assert
        Mockito.verify(userMockRepository, Mockito.times(1)).create(newUser);
    }

    @Test(expected = IllegalArgumentException.class)
    public void create_Should_ThrowException_When_UsernameIsNotUnique() {
        // Arrange
        User newUser = new User(1, "petko", "1234567", "petko@abv.bg", "");
        Mockito.when(userMockRepository.getAll())
                .thenReturn(Arrays.asList(
                        new User(1, "petko", "1234567", "petko@abv.bg", ""),
                        new User(2, "petko12", "123456789", "petko23@abv.bg", ""),
                        new User(3, "petko56", "1234567", "petko164343@abv.bg", "")
                ));

        // Act
        userService.create(newUser);

        // Assert
        Mockito.verify(userMockRepository, Mockito.never()).create(newUser);
    }

    @Test
    public void delete_Should_CallRepositoryDelete_When_RepoHasUsername() {
        // Arrange
        User newUser = new User(1, "petko", "1234567", "petko@abv.bg", "");

        // Act
        userService.delete("petko");

        // Assert
        Mockito.verify(userMockRepository, Mockito.times(1)).delete("petko");
    }

    @Test(expected = IllegalArgumentException.class)
    public void delete_Should_ThrowException_When_RepoDoesntHaveUsername() {
        // Arrange
        User newUser = new User(1, "petko", "1234567", "petko@abv.bg", "");

        Mockito.when(userMockRepository.getByUserName("petko"))
                .thenReturn(null);

        // Act
        userService.getByName("petko");
    }



    @Test(expected = IllegalArgumentException.class)
    public void delete_Should_ThrowException_When_RepoDoesntHaveUserId() {
        // Arrange
        User newUser = new User(1, "petko", "1234567", "petko@abv.bg", "");

        Mockito.when(userMockRepository.getById(100))
                .thenReturn(null);

        // Act
        userService.getById(100);
    }





}



