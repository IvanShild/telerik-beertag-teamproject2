package com.beertag.project.mockobjects.controllersTests;

import com.beertag.project.models.Beer;
import com.beertag.project.models.Style;
import com.beertag.project.services.BeerService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class BeerControllerTests {
    @MockBean
    BeerService beerServiceMock;

    @Autowired
    MockMvc mockMvc;

    @Test
    public void getById_Should_ReturnStatusOK_When_UserExists() throws Exception {
        // Arrange
        Mockito.when(beerServiceMock.getById(1))
                .thenReturn(new Beer(1, "Pirinsko", "", 5, 10, "", new Style()));

        // Act, Arrange
        mockMvc.perform(MockMvcRequestBuilders.get("/api/beers/{id}", 1))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("@.name").value("Name"))
                .andExpect(MockMvcResultMatchers.jsonPath("@.id").value(1));
    }

    @Test
    public void getById_Should_ReturnStatusNOT_FOUND_When_UserNotExists() throws Exception {
        // Arrange
        Mockito.when(beerServiceMock.getById(1))
                .thenThrow(IllegalArgumentException.class);

        // Act, Arrange
        mockMvc.perform(MockMvcRequestBuilders.get("/api/beers/{id}", 1))
                .andExpect(MockMvcResultMatchers.status().isNotFound());
    }
}


