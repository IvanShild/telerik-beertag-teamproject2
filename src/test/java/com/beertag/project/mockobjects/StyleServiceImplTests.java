package com.beertag.project.mockobjects;

import com.beertag.project.models.Style;
import com.beertag.project.repositories.StyleRepository;
import com.beertag.project.services.StyleServiceImpl;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class StyleServiceImplTests {

    @Mock
    private StyleRepository styleMockRepository;

    @InjectMocks
    private StyleServiceImpl styleService;

    @Test
    public void getById_Should_ReturnStyle_When_StyleWithSameIdExists() {
        // Arrange
        Mockito.when(styleMockRepository.getById(1))
                .thenReturn(new Style(1,"Light"));

        // Act
        Style result = styleService.getById(1);

        // Assert
        Assert.assertEquals(1, result.getStyleId());
    }

    @Test(expected = IllegalArgumentException.class)
    public void getById_Should_ThrowException_When_StyleNotExists() {
        // Arrange
        Mockito.when(styleMockRepository.getById(100))
                .thenReturn(null);

        // Act
        styleService.getById(100);
    }
}
