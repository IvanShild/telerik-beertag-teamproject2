package com.beertag.project.mockobjects;

import com.beertag.project.models.Tag;
import com.beertag.project.repositories.BeerRepository;
import com.beertag.project.repositories.TagRepository;
import com.beertag.project.services.TagServiceImpl;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Arrays;

@RunWith(MockitoJUnitRunner.class)
public class TagServiceImplTests {
    @Mock
    private TagRepository tagMockRepository;
    @Mock
    private BeerRepository beerMockRepository;

    @InjectMocks
    private TagServiceImpl tagService;

    @Test
    public void create_Should_CallRepositoryCreate_When_TagNameIsUnique() {
        // Arrange
        Tag newTag = new Tag(1, "fresh");

        // Act
        tagService.create(newTag);

        // Assert
        Mockito.verify(tagMockRepository, Mockito.times(1)).create(newTag);
    }

    @Test(expected = IllegalArgumentException.class)
    public void create_Should_ThrowException_When_BeerNameIsNotUnique() {
        // Arrange
        Tag newTag = new Tag(1, "fresh");
        Mockito.when(tagMockRepository.getAll())
                .thenReturn(Arrays.asList(
                        new Tag(1, "fresh"),
                        new Tag(2, "cold"),
                        new Tag(3, "sweet")

                ));


        // Act
        tagService.create(newTag);

        // Assert
        Mockito.verify(tagMockRepository, Mockito.never()).create(newTag);
    }

    @Test
    public void getById_Should_ReturnStyle_When_StyleWithSameIdExists() {
        // Arrange
        Mockito.when(tagMockRepository.getById(1))
                .thenReturn(new Tag(1, "fresh"));

        // Act
        Tag result = tagService.getById(1);

        // Assert
        Assert.assertEquals(1, result.getTagId());
    }

    @Test(expected = IllegalArgumentException.class)
    public void getById_Should_ThrowException_When_StyleNotExists() {
        // Arrange
        Mockito.when(tagMockRepository.getById(1))
                .thenReturn(null);

        // Act
        tagService.getById(1);
    }

    @Test(expected = IllegalArgumentException.class)
    public void removeTagFromBeer() {
        Mockito.when((beerMockRepository.getById(1).getTags()))
                .thenReturn(null);

        tagService.getById(1);
    }

}
