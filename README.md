# Telerik-BeerTag-TeamProject2

**TeamProject
Penyo Krotev & Ivan Shilderov
Trello board:** [click](https://trello.com/b/lmhcEYb1/beer-tag)


Beer Tag

Team Project Assignment

Java Team Project

This document describes a sample team project assignment for the Java cohort at Telerik Academy.

Project Description

Your task is to develop BEER TAG web application. BEER TAG enables your users to manage all the beers that they have drank and want to drink. Each beer has detailed information about it from the ABV (alcohol by volume) to the style and description. Data is community driven and every beer lover can add new beers and edit missing information on already existing ones. Also, BEER TAG allows you to rate a beer and calculates average rating from different users.

Project Requirements

Note: The document contains mocks that are used for visualizing the described functionality. Even though the team might choose to implement a similar design, the mocks should not be interpreted as design requirements and constraints.

Web application

Public Part

The public part of your projects should be visible without authentication. This includes the application start page, the user login and user registration forms, as well as list of all beers that have been added from different users. People that are not authenticated can not see any user specific details, neither they can interact with the website, they can only browse the beers and see list and details of them.

Private Part (Users only)

Registered users should have private part in the web application accessible after successful login.

The web application provides them with UI to add/edit/delete beers.

Each beer has name, brewery that produces it, origin country, ABV, description, style (pre-defined) and picture. Users can add tags to each beer. They can select among already added or create new. Each beer can be marked as “drank” and “want to drink”. When beer is marked as drank message should appear with some text, for example “Cheers!”.

Each user can rate a beer and average rating is calculated. Both ratings should be visible for the users.

All beers are listed and can be sorted by rating, ABV, alphabetically. The list can be filtered by tags, style, origin country.

There should be master-details view that will show beer details when one is selected.

It is your choice of design where and what properties to visualize, but all data should be visible somewhere.

The app should have profile page that shows user’s photo and name from their contact on the device and their top 3 most ranked beers, they’ve tasted.

Administration Part

System administrators should have administrative access to the system and permissions to administer all major information objects in the system, e.g. to create/edit/delete users and other administrators, to edit/delete beers and related data if they decide to. Web Application

Behind every great web application is a great backend. And the first impression your application creates is via the frontend.

It is your choice whether to use Spring REST API + JavaScript UI (single page app) or Spring MVC + Thymeleaf to create it.

Don’t forget to follow good coding practices when implementing the REST API or the MVC pattern.

Database

All BEER TAG data should be stored in MySQL database.

Technical Requirements

Your project should meet the following requirements (these requirements will be used by TA trainers during project defense):

 Use IntelliJ IDEA

 For the web application (UI) you can choose from two approaches o Use Spring MVC Framework with Thymeleaf template engine for generating the UI o Research and build Single Page Application to serve the UI with JavaScript library of your choice. Please, keep in mind that we have not covered this in our sessions, so it is totally up to you how to implement the authentication and other SPA specifics that depend on the library you choose.

 Use AJAX form communication in some parts of your application

 Use Dependency Inversion principle and Dependency Injection technique following the best practices

 Use MySQL (MariaDB) as database back-end o Use Hibernate to access your database

 If you do the research, you may use JPA or some other Hibernate-based framework.

o Using repositories and/or service layer is a must

 Create tables with data with server-side paging and sorting for every model entity o You can use JavaScript library or generate your own HTML tables

 Create beautiful and interactive web UI o You may use Bootstrap or Materialize o You may change the standard theme and modify it to apply own web design and visual styles

 Apply error handling and data validation to avoid crashes when invalid data is entered (both client-side and server-side)

 Use Spring Security for managing users and roles o Your registered users should have at least one of the two roles: user and administrator

o If you’ve chosen to develop SPA, you’d need to implement JWT Authentication.

 Create unit tests for your "business" functionality following the best practices for writing unit tests (at least 80% code coverage)

 Follow OOP best practices and SOLID principles.

 Use Git & GitLab for source control management – be careful with commit messages

 Use Trello for project management

 Create user documentation / manual of the application

Optional Requirements (bonus points)

 Integrate your app with a Continuous Integration server (e.g. Jenkins or other). Configure your unit tests to run on each commit to your master branch

 Host your application’s backend in a public hosting provider of your choice (e.g. AWS, Azure)

 Use Dependency Inversion principle and Dependency Injection technique following the best practices (optional)

Guidelines (recommended steps)

 Create an Object-Oriented design of your application o What are the models in your application i.e. User, Beer

o What are the relationships between them i.e. Beer has owner (User), User has a Wish List (Beers)

 Create REST API that implements all business logic o You can begin with in-memory data and add database later

o You can implement authentication by passing username in request headers (we will provide you with a demo)

 Cover functionality (service layer) with unit tests

 Integrate your application with a database

 Create UI for your application o Chose approach (MVC or REST+SPA)

o In either case keep the REST service working

 Implement security mechanism o Store users and passwords in database (passwords are encrypted) o Implement Spring Security (for MVC) or JWT authentication (for SPA) Deliverables

Provide link to a Git repository with the following information:

 Link to the Trello board

 Project documentation

 Screenshots of your application

 URL of the application (if hosted online)

Public Project Defense

Each student must make a public defense of its work to the trainers, Partner and students (~30-40 minutes). It includes:

 Live demonstration of the developed web application (please prepare sample data).

 Explain application structure and its source code